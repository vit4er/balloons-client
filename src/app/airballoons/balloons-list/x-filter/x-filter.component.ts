import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-x-filter',
  templateUrl: './x-filter.component.html',
  styleUrls: ['./x-filter.component.css']
})
export class XFilterComponent implements OnInit {
  @Output() filterSet: EventEmitter<any> = new EventEmitter<any>();

  disabled: boolean;
  comparSigns = [
    'fa fa-greater-than',
    'fa fa-greater-than-equal',
    'fa fa-less-than',
    'fa fa-less-than-equal',
    'fa fa-equals',
    'fa fa-not-equal'
  ];
  currentSign: string;
  value: number;
  filterMatchMode: string;

  constructor() {
  }

  ngOnInit() {
    this.disabled = true;
    this.currentSign = 'fa fa-greater-than';
    this.value = 0;
  }

  changeSign() {
    let i = this.comparSigns.indexOf(this.currentSign);
    i++;
    this.currentSign = (i > this.comparSigns.length - 1) ? this.comparSigns[0] : this.comparSigns[i];
  }

  clearInput() {
    this.value = 0;
    this.currentSign = this.comparSigns[0];
  }

  setFilter() {
    this.disabled = !this.disabled;
    if (!this.disabled) {
      switch (this.currentSign) {
        case 'fa fa-equals': { this.filterMatchMode = 'equals'; } break;
        case 'fa fa-not-equal': { this.filterMatchMode = 'notEquals'; } break;
        case 'fa fa-greater-than': { this.filterMatchMode = 'gt'; } break;
        case 'fa fa-greater-than-equal': { this.filterMatchMode = 'gte'; } break;
        case 'fa fa-less-than': { this.filterMatchMode = 'lt'; } break;
        case 'fa fa-less-than-equal': { this.filterMatchMode = 'lte'; } break;
        default: { this.filterMatchMode = 'equals'; } break;
      }
      this.filterSet.emit( {
        value: this.value,
        matchMode: this.filterMatchMode
      });
    } else {
      this.filterSet.emit( {
        value: null,
        matchMode: null
      });
    }
  }
}
