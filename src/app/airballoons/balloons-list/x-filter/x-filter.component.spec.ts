import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XFilterComponent } from './x-filter.component';

describe('XFilterComponent', () => {
  let component: XFilterComponent;
  let fixture: ComponentFixture<XFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
